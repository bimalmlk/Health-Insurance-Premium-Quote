package org.emids.controller;

import java.util.ArrayList;
import java.util.List;

import org.emids.model.Customer;
import org.emids.service.PremiumService;
import org.emids.service.impl.PremiumServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
/*
 * This controller class for handling the request from UI.
 */
@Controller
public class CustomerPremiumController {
	
/*
 * This method for premium form.
 */
	   @RequestMapping(value = "/customer.html", method = RequestMethod.GET)
	   public ModelAndView customerPremiumCalForm() {
		   Customer customer = new Customer();
		   customer.setGender("M");
		  ModelAndView modelAndView = new ModelAndView("index", "command", customer);
		  return modelAndView;
	   }
	   /*
	    * This method handles for premium calculation business logic.
	    */
	   @RequestMapping(value = "/premium.html", method = RequestMethod.POST)
	   public String premium(@ModelAttribute("Customer")Customer customer, ModelMap model) {
		   String str= "";
		   if(customer.getGender().equalsIgnoreCase("M")){
			   str = "Mr.";
		   }else
			   if(customer.getGender().equalsIgnoreCase("F")){
			   str = "Mrs.";
		   }else {
			   str = "";
		   }
		   PremiumService customerrPre = new PremiumServiceImpl();
		  String premium = customerrPre.getPremium(customer);
	      model.addAttribute("name", customer.getName());
	      model.addAttribute("premium", premium);
	      model.addAttribute("greet", str);
	      return "customer";
	   }
	   /*
	    * Adding this list customer health status objects added into UI for check boxes.
	    */
	   @ModelAttribute("customerHealthStatusList")
	   public List<String> getCustomerHealthStatus()
	   {
	      List<String> customerHealthStatus = new ArrayList<String>();
	      customerHealthStatus.add("Hypertension");
	      customerHealthStatus.add("Blood pressure");
	      customerHealthStatus.add("Blood sugar");
	      customerHealthStatus.add("Overweight");
	      return customerHealthStatus;
	   }
	 /*
	  * Adding this list customer health status objects added into UI for check boxes.
	  */
	   @ModelAttribute("customerHabitsList")
	   public List<String> getCustomerHabits()
	   {
	      List<String> customerHabits = new ArrayList<String>();
	      customerHabits.add("Smoking");
	      customerHabits.add("Alcohol");
	      customerHabits.add("Daily Exercise");
	      customerHabits.add("Drugs");
	      return customerHabits;
	   }

}
