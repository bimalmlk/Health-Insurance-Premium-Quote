package org.emids.model;
/*
 * This model class for customer informations
 */
public class Customer {
	private String name;
	private String gender;
	private int age;
	String[] customerHealth;
	
	
	
	String[] customerHabit;


	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getGender() {
		return gender;
	}



	public void setGender(String gender) {
		this.gender = gender;
	}



	public int getAge() {
		return age;
	}



	public void setAge(int age) {
		this.age = age;
	}



	public String[] getCustomerHealth() {
		return customerHealth;
	}



	public void setCustomerHealth(String[] customerHealth) {
		this.customerHealth = customerHealth;
	}



	public String[] getCustomerHabit() {
		return customerHabit;
	}



	public void setCustomerHabit(String[] customerHabit) {
		this.customerHabit = customerHabit;
	}

}
