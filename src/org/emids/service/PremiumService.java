package org.emids.service;

import org.emids.model.Customer;

public interface PremiumService {
	/*
	 * This method used for getting premium from business class and converting to String in Indian currency format.
	 */
	public String getPremium(Customer customer);
}
