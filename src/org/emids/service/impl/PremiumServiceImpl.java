package org.emids.service.impl;

import org.emids.model.Customer;
import org.emids.premium.cal.PremiumBusinessLogic;
import org.emids.service.PremiumService;

public class PremiumServiceImpl implements PremiumService{

	
	
	
	
	
	@Override
	public String getPremium(Customer customer) {
		PremiumBusinessLogic pbl = new PremiumBusinessLogic();
		double premiumValue = pbl.customerPremiumCal(customer);
		String premiumWithD = premiumValue+"";
		String premium = premiumWithD.substring(0, premiumWithD.indexOf("."))+"/-";
		return premium;
	}

}
