package org.emids.premium.cal;

import org.emids.model.Customer;
/*
 * This class handles business logics for calculating premium based on 
 * age, gender, current health status, Good and bad habits.
 */
public class PremiumBusinessLogic {
	static double basePre = 5000;
	/*
	 * This method is actually calculating the premium as per business.
	 */
	public double customerPremiumCal(Customer customer) {
		double premium = 0.0;
		int age = customer.getAge();
		
		
		if(age<18){
			premium = 5000;
			double fpremium = premiumBasedonCustomer(customer, premium);
			premium = Math.round(premiumbasedOnHabit(customer, fpremium));
			if(premium<basePre){
				premium = basePre;
			}
		}
		if(age>=18 && age<25)
		{
			double premiumT = preimium(customer, basePre);
			premium = premiumBasedOnGender(customer, premiumT);
		}
		if(age>=25 && age<30){
			double preBellowTwentyFive = preimium(customer, basePre);
			premium = preimium(customer, preBellowTwentyFive);
			premium = premiumBasedOnGender(customer, premium);
		}
		if(age>=30 && age<35){
			double preBellowThirtyFive = preimium(customer, basePre);
			double premiumThirtyFive = preimium(customer, preBellowThirtyFive);
			double premiumTh = preimium(customer, premiumThirtyFive);
			premium = premiumBasedOnGender(customer, premiumTh);
		}
		if(age>=35 && age<40){
			double preBellowThirtyFive = preimium(customer, basePre);
			double premiumThirtyFive = preimium(customer, preBellowThirtyFive);
			double premiumFourty = preimium(customer, premiumThirtyFive);
			double premiumTh = preimium(customer, premiumFourty);
			premium = premiumBasedOnGender(customer, premiumTh);
		}
		if(age>=40){
			double preBellowThirtyFive = preimium(customer, basePre);
			double premiumThirtyFive = preimium(customer, preBellowThirtyFive);
			double premiumFourty = preimium(customer, premiumThirtyFive);
			double premiumaboveFourty = preimium(customer, premiumFourty);
			double premiumTh = preimium(customer, premiumaboveFourty);
			premium = premiumBasedOnGender(customer, premiumTh);
		}
		return premium;
	}

	private double premiumbasedOnHabit(Customer customer, double premiumHealth) {
		double premium=premiumHealth;
		double bper = 0.0;
		double gper = 0.0;
		String[] customerHabits = customer.getCustomerHabit();
		for(String habits:customerHabits){
		
			if(habits.equalsIgnoreCase("Smoking")||habits.equalsIgnoreCase("Alcohol")
					||habits.equalsIgnoreCase("Drugs")){
				bper =  premiumHealth * 3/100;
				premium = premium + bper;
			}
			if(habits.equalsIgnoreCase("Daily exercise")){
				gper =  premiumHealth * 3/100;
				premium = premium -gper;
			}
		}
		return premium;
	}

	private double premiumBasedonCustomer(Customer customer, double premiumTh) {
		double premium=premiumTh;
		String[] currHealth = customer.getCustomerHealth();
		for(String cHealth:currHealth){
			if(cHealth.equalsIgnoreCase("Hypertension")||cHealth.equalsIgnoreCase("Blood pressure")
					||cHealth.equalsIgnoreCase("Blood sugar")||cHealth.equalsIgnoreCase("Overweight")){
				premium = customerHealthbasicPremium(cHealth, premium);
			}else{
				premium = premiumTh;
			}
			
		}
		return premium;
	}

	private double preimium(Customer customer, double basePre) {
		double increaseInPer = basePre+(basePre *10/100);
		if(customer.getAge()>=40){
			increaseInPer = basePre+(basePre *20/100);
		}
		return increaseInPer;
		
	}

	private double customerHealthbasicPremium(String cHealth, double premium) {
		double hPremium = 0.0;
		if(cHealth.equalsIgnoreCase("Hypertension")){
			double hPercentage = premium *1/100;
			hPremium = premium + hPercentage;
		}
		else{
			hPremium = premium;
		}
		if(cHealth.equalsIgnoreCase("Blood pressure")){
			double hPercentage = hPremium *1/100;
			hPremium = hPremium + hPercentage;
		}
		if(cHealth.equalsIgnoreCase("Blood sugar")){
			double hPercentage = hPremium *1/100;
			hPremium = hPremium + hPercentage;
		}
		if(cHealth.equalsIgnoreCase("Overweight")){
			double hPercentage = hPremium *1/100;
			hPremium = hPremium + hPercentage;
		}
		return hPremium;
	}
	private double premiumBasedOnGender(Customer customer,double prevPremium){
		double premium =0;
		if(customer.getGender().equalsIgnoreCase("M")){
			double mPrec = prevPremium  * 2/100;
			double mPre = prevPremium + mPrec;
			double premiumHealth = premiumBasedonCustomer(customer, mPre);
			premium  = Math.round(premiumbasedOnHabit(customer, premiumHealth));
		}else{
			double fpremium = premiumBasedonCustomer(customer, prevPremium);
			premium = Math.round(premiumbasedOnHabit(customer, fpremium));
		}
		if(premium<basePre){
			premium = basePre;
		}
		return premium;
	}
}
