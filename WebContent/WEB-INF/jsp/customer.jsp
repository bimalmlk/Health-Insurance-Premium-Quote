<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>
 <style type="text/css">
      *{
             font-family: monospace;
       }
       </style>
<title>Health Insurance</title>
</head>
<body>
<div class="container">
<h2>Premium</h2>
   <table class="table table-bordered">
      <tr>
         <td align="center"><h2>Health Insurance Premium for ${greet} ${name} is: Rs. ${premium}</h2></td>
      </tr>
   </table>  
   <a href="/health_insurance_premium_quote_generator/customer.html" class="btn btn-lg btn-primary">Back</a>
   </div>
</body>
</html>