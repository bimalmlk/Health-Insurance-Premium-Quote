<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<title>Insurance Quote Generator</title>
<style type="text/css">

      *{
             font-family: monospace;
       }
       input[type="radio"]{
  margin: 0 10px 0 10px;
}
         input[type="checkbox"]{
  margin: 0 5px 0 5px;
} 

</style>
<script type="text/javascript">
function validate()
{
    var a = document.getElementById("name");
    var b = document.getElementById("age");
	var d = document.getElementById("age").value;
    var valid = true;
    if(a.value.length<=0 || b.value.length<=0)
        {
            alert("Don't leave the field empty!");
            valid = false;
        }
        else{
            if(isNaN(d)){
                alert("Enter a number");
        valid = false;
        }else
        	if(d<=0){
        		alert("Age must be greater than 0");
        		valid = false
        }else{
        	if(d>=60){
        		alert("Age must be less than 60");
        		valid = false
        	}
        }
    }
    return valid;
};
</script>
</head>
<body>
<div class="container">
<form:form method = "POST" action = "/health_insurance_premium_quote_generator/premium.html" onsubmit="return validate();">
        <center><h1>Insurance Quote Generator</h1></center>
         <table class="table table-bordered table-hover table-striped">
         
            <tr>
               <td class="form-group"><form:label path = "name">Name</form:label></td>
               
               <td><div class="col-xs-8"><form:input path = "name"  id="name"/></div></td>
               
            </tr>
            <tr>
               <td class="form-group"><form:label path = "age">Age</form:label></td>
               
               <td> <div class="col-xs-8"><form:input path = "age" id="age"/></div></td>
            </tr>
            <tr>
         <td class="form-group"><form:label path="gender">Gender</form:label></td>
         <td><div class="col-xs-8">
            <form:radiobutton path="gender" value="M" label="Male" />
            <form:radiobutton path="gender" value="F" label="Female" />
             <form:radiobutton path="gender" value="O" label="Other" /></div>
         </td>
      </tr>
            <tr>
               <td class="form-group"><form:label path="customerHealth">Current Health</form:label></td>
         		<td><div class="col-xs-8"><form:checkboxes items="${customerHealthStatusList}" path="customerHealth"/>
         		</div></td>       
            </tr>
              <tr>
               <td class="form-group"><form:label path="customerHabit">Habits</form:label></td>
         		<td><div class="col-xs-8"><form:checkboxes items="${customerHabitsList}" path="customerHabit" />
         		</div></td>       
            </tr>
           
         </table> 
         <center>
          <button class="btn btn btn-primary"  type="submit">Check</button> </center>
      </form:form>
      </div>
</body>
</html>
